const mysql = require("mysql");
const yaml = require("yamljs");
const config = yaml.load("./config.yaml");
const env = process.env.ENV || "development";

class DatabaseConnector {
    constructor(config) {
        this.connection = mysql.createConnection({
            host: config[env].host,
            user: config[env].username,
            password: config[env].password,
            database: config[env].database,
            port: config[env].port,
            timezone: config[env].tz
        });
    }

    query(sql, args) {
        return new Promise((resolve, reject) => {
            this.connection.query(sql, args, (err, rows) => {
                if (err) return reject(err);
                resolve(rows);
            });
        });
    }

    close() {
        return new Promise((resolve, reject) => {
            this.connection.end(err => {
                if (err) return reject(err);
                resolve();
            });
        });
    }
}

module.exports = new DatabaseConnector(config);
