const express = require("express");
const app = express();

const Command = require('./src/Command');
const MessageProducer = require('./src/MessageProducer');
const MessageConsumer = require('./src/MessageConsumer');

MessageConsumer.listen('meter-read');
MessageProducer.sendMessage('meter-read', Command.Meter.Read('12345'));

app.listen(8400);