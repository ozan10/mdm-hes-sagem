module.exports = {
    Subject: {
        METER_READ_REQUEST: "MDMS_METER_READINGS_QUEUE",
        METER_READ_RESPONSE: "HES_MDMS_METER_READS_REPLY"
    },
    Meter: {
        Read: function (mRID, dateTime) {
            const query = '   <?xml version="1.0"encoding="UTF-8"?>  ' +
                '   <MeterReadsRequestMessage  ' +
                '   	xmlns="http://www.emeter.com/energyip/amiinterface">  ' +
                '   	<header>  ' +
                '   		<verb>request</verb>  ' +
                '   		<noun>MeterReads</noun>  ' +
                '   		<revision>1</revision>  ' +
                '   		<dateTime>2017-05-  13T01:55:51-08:00</dateTime>  ' +
                '   		<source>MDMS</source>  ' +
                '   		<messageID>353</messageID>  ' +
                '   	</header>  ' +
                '   	<payload>  ' +
                '   		<startTime>2017-05-  11T01:55:51-08:00</startTime>  ' +
                '   		<Meter>  ' +
                '   			<mRID>' + mRID + '</mRID>  ' +
                '   			<idType>METER_X_UDC_ASSET_ID</idType>  ' +
                '   		</Meter>  ' +
                '   		<requestPriority>1</requestPriority>  ' +
                '   		<measurementProfile>PQ1</measurementProfile>  ' +
                '   		<retrieveRegisterReads>false</retrieveRegisterReads>  ' +
                '   		<retrieveIntervalReads>false</retrieveIntervalReads>  ' +
                '   		<demandReset>false</demandReset>  ' +
                '   		<retrieveEvents>false</retrieveEvents>  ' +
                '   		<executeStartTime>2017-05-  01T00:00:00-08:00</executeStartTime>  ' +
                '   		<executeExpireTime>2017-06-  01T00:00:00-08:00</executeExpireTime>  ' +
                '   	</payload>  ' +
                '  </MeterReadsRequestMessage>  ';
            return query;
        },

        Response: function (mRID) {
            return
            const query = '   <?xml version="1.0"encoding="UTF-8"?>  ' +
                '   <MeterReadsReplyMessage  ' +
                '   	xmlns="http://www.emeter.com/energyip/amiinterface">  ' +
                '   	<header>  ' +
                '   		<verb>reply</verb>  ' +
                '   		<noun>MeterReads</noun>  ' +
                '   		<revision>1</revision>  ' +
                '   		<dateTime>2017-12-  23T00:31:01-08:00</dateTime>  ' +
                '   		<source>SICONIA</source>  ' +
                '   	</header>  ' +
                '   	<payload>  ' +
                '   		<MeterReading>  ' +
                '   			<Meter>  ' +
                '   				<mRID>' + mRID + '</mRID>  ' +
                '   				<idType>METER_X_UDC_ASSET_ID</idType>  ' +
                '   			</Meter>  ' +
                '   			<IntervalBlock>  ' +
                '   				<readingTypeId>P15MIN_1-0:32.24.0.255</readingTypeId>  ' +
                '   				<IReading>  ' +
                '   					<endTime>2017-12-  22T18:00:00-08:00</endTime>  ' +
                '   					<value>5</value>  ' +
                '   					<collectionTime>2017-12-  23T00:00:00-08:00</collectionTime>  ' +
                '   				</IReading>  ' +
                '   			</IntervalBlock>  ' +
                '   			<IntervalBlock>  ' +
                '   				<readingTypeId>P15MIN_1-0:52.24.0.255</readingTypeId>  ' +
                '   				<IReading>  ' +
                '   					<endTime>2017-12-  22T18:00:00-08:00</endTime>  ' +
                '   					<intervalLength>3600</intervalLength>  ' +
                '   					<value>5</value>  ' +
                '   					<flags>0</flags>  ' +
                '   					<collectionTime>2017-12-  23T00:00:00-08:00</collectionTime>  ' +
                '   				</IReading>  ' +
                '   				<IReading>  ' +
                '   					<endTime>2017-12-  22T19:00:00-08:00</endTime>  ' +
                '   					<intervalLength>3600</intervalLength>  ' +
                '   					<value>6</value>  ' +
                '   					<flags>0</flags>  ' +
                '   					<collectionTime>2017-12-  23T00:00:00-08:00</collectionTime>  ' +
                '   				</IReading>  ' +
                '   			</IntervalBlock>  ' +
                '   			<IntervalBlock>  ' +
                '   				<readingTypeId>P15MIN_1-0:72.24.0.255</readingTypeId>  ' +
                '   				<IReading>  ' +
                '   					<endTime>2017-12-  22T19:00:00-08:00</endTime>  ' +
                '   					<intervalLength>3600</intervalLength>  ' +
                '   					<value>3.365</value>  ' +
                '   					<flags>0</flags>  ' +
                '   					<collectionTime>2017-12-  23T00:00:00-08:00</collectionTime>  ' +
                '   				</IReading>  ' +
                '   			</IntervalBlock>  ' +
                '   		</MeterReading><  /payload>  ' +
                '   		<reply>  ' +
                '   			<replyCode>0</replyCode>  ' +
                '   			<correlationId>353</correlationId>  ' +
                '   		</reply>  ' +
                '  	</MeterReadsReplyMessage>   ';

            return query;
        }
    }
}