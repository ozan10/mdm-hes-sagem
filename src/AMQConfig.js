const config = require("yamljs").load("./config.yaml");
const AMQ = "AMQ";

module.exports = {
    host: config[AMQ].host,
    port: config[AMQ].port,
    connectHeaders: {
        host: '/',
        login: config[AMQ].user,
        passcode: config[AMQ].password,
        'heart-beat': '5000,5000'
    }
};