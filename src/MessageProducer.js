const AMQConnector = require('./AMQConnector');

const MessageProducer = function MessageProducer() { };

MessageProducer.prototype.sendMessage = (destination, messageToPublish) => {
    AMQConnector.connect((error, client) => {
        if (error) {
            console.error(error);
            return;
        }

        const sendHeaders = {
            'destination': destination,
            'content-type': 'text/plain',
            'verb': 'request'
        };

        const frame = client.send(sendHeaders);
        frame.write(messageToPublish);
        frame.end();
        //client.disconnect();
    })

};

module.exports = new MessageProducer();