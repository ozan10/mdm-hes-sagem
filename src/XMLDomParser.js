var parseString = require('xml2js').parseString;
const util = require('util');

let XMLParser = function XMLParser() { };

XMLParser.prototype.parse = (xml, callback) => {
    return parseString(xml, callback);
};

XMLParser.prototype.print = (message) => {
    return util.inspect(message, false, null);
}

module.exports = new XMLParser();