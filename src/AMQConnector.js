const stompit = require('stompit');
const AMQConfig = require('./AMQConfig.js');

let AMQConnector = function AMQConnector() {
    this._client = null;
}

AMQConnector.prototype.connect = (callback) => {
    stompit.connect(AMQConfig, callback);
}



module.exports = new AMQConnector();