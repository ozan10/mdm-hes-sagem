const config = require("yamljs").load("./config.yaml");
const util = require('util');
const AMQConnector = require('./AMQConnector');
const XMLParser = require('./XMLDomParser');


let MessageConsumer = function MessageConsumer() {
};

MessageConsumer.prototype.listen = (destinationName) => {
    AMQConnector.connect((error, client) => {
        if (error) {
            console.error(error);
            return;
        }

        const subscribeHeaders = {
            'destination': destinationName,
            'ack': 'client-individual'
        }

        client.subscribe(subscribeHeaders, (error, message) => {
            if (error) {
                console.error(error);
                return;
            }

            message.readString('utf-8', (error, body) => {
                if (error) {
                    console.error(error);
                    return;
                }

                XMLParser.parse(body, (error, result) => {
                    if (error) {
                        console.log("Error ", error);
                        return;
                    }

                    console.log("\nReceived Message :")
                    console.log(XMLParser.print(result));
                    console.log("\n");
                })

                client.ack(message);
            })
        })
    })
}

module.exports = new MessageConsumer();